#!/bin/sh

set -e

working_dir=$(pwd)
project_dir=$(dirname $(dirname $(realpath $0)))

cd $project_dir


uv pip compile --universal -p 3.10 --annotation-style=line \
    --upgrade-package nomad-lab \
    --upgrade-package nomad-plugin-gui \
    --extra=plugins \
    --output-file=requirements.txt \
    pyproject.toml

