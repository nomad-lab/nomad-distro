## nomad-distro

nomad-distro handles deployments for Central NOMAD and related environments. It allows plugin configuration, nightly updates, and manual deployments for staging and production.

## Deployment Categories

### 1. **Central Deployment**

The central deployment serves **production purposes**. Its focus is on stable releases that have undergone thorough testing. It comprises four sub-environments:

- **Develop**: The starting point for updates, automatically updated nightly to incorporate the latest changes in `nomad-lab` package.
- **Staging**: A pre-production environment for testing stable releases before full deployment.
- **Testing**: An intermediate environment used for quality assurance of stable builds.
- **Production**: The final environment where all users access stable and tested releases.

Central deployments rely on the **develop branch** in GitLab.

### 2. **Oasis Deployment**

The oasis deployment is a **testbed for plugin development**, where plugins are continuously updated and tested. This environment allows rapid iteration and experimentation to ensure that new plugins or updates work well before promotion to the central deployment.

Oasis deployments rely on the **test-oasis branch** in GitLab.

## Steps for Deployment

1. Configure Plugins:

   a. Update the plugins in pyproject.toml.

   b. Generate the requirements file:

```bash
    ./scripts/generate_python_dependencies.sh
```

2. Create Merge Requests (MRs):

- For Central NOMAD: MR to the main branch.
- For Test-Oasis: MR to the test-oasis branch.

### Nightly Deployments (Automatic):

    Updates nomad-lab package and redeploys nightly to develop and test-oasis.

### Manual Deployments:

    Staging/Production/Testing: Trigger manually after new releases.

| Deployment Environment | Branch       | Update Method | Purpose                                     | Link
| ---------------------- | ------------ | ------------- | ------------------------------------------- | -----------------------------------------
| **Develop**            | `develop`    | Nightly       | Initial testing and development of changes. | https://nomad-lab.eu/prod/v1/develop/gui/
| **Staging**            | `develop`    | Manual        | Pre-production testing of stable builds.    | https://nomad-lab.eu/prod/v1/staging/gui/
| **Testing**            | `develop`    | Manual        | QA and validation of stable builds.         | https://nomad-lab.eu/prod/v1/test/gui/
| **Production**         | `develop`    | Manual        | Final, stable environment for all users.    | https://nomad-lab.eu/prod/v1/gui
| **Oasis**              | `test-oasis` | Nightly       | Plugin development and testing.             | https://nomad-lab.eu/prod/v1/oasis/gui/
